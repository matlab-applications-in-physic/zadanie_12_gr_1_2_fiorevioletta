% Program tested in MatLab R2019b
% Creator: Julia Pluta
% Code credits: gitlab.com/@rafalosa
% All Contents 2020 Copyright Julia Pluta All rights reserved

%Physical constants and task data:
tau = 2;
G = 79e9;
g = 9.807;
R = 2 * 2.54e-2;
r = 2e-3;
m = 1e-2;
N = 2:1:50; 
freq = (0:0.01:400);

%Values calculated from formulas:
f = m * g;
alpha = f/m;
beta = 1/(2 * tau);


for i = 1:size(N,2)
    %Spring constant
    k = G * r^4/(R^3 * N(i) * 4); 
    %Resonant frequency
    omega = sqrt(k / m); 
    %Calculate the amplitude and resonance frequency
    for j = 1:size(freq,2)
        A(j,i) = alpha/sqrt((omega^2 - freq(j)^2)^2 + (4 * beta^2 * freq(j)^2));
    end
    [peak(i), index] = max(A(:,i));
    psi(i) = freq(index)/2/pi;
    %Creating the first chart.
    subplot(2,1,1); 
    plot(freq/2/pi,A)
end

%Giving the parameters of the X axis and signing the axis in the first chart.
xlim([0 80])
xlabel('Frequency [Hz]');
ylabel('A [m]');

%Create a second chart.
subplot(2,1,2);
plot(N,peak)

%Giving the parameters of the X axis and signing the axis in the second chart.
xlim([0 55]);
xlabel('N');
ylabel('Max A [m]');

%Saving graphs to a file.
saveas(gcf,'resonance.pdf');

%Creating a text file.
field = fopen('resonance_analysis_results.dat','w');

%Print constants in a text file.
fprintf(field,'R = %.4f [m], r = %.3f [m], m = %.2f [kg], tau = %.0f [s], g = %.3f [m/s^2], G = %d [Pa] \n',R,r,m,tau,g,G);

%Output of curve values in a text file.
for i = 1:size(N,2)
    fprintf(field,'N = %.0f, A = %.4f [m], f = %.4f [Hz]\n',N(i),peak(i),psi(i));
end

%Close the text file.
fclose(field);